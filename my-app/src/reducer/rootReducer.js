import {combineReducers} from 'redux'
import {cardReducer} from '../component/reducer'

export default combineReducers({
    cardReducer
});
