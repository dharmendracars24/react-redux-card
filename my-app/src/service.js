import axios from "axios";

export const BASE_API_URL = 'https://swapi.co/api/'

const api = axios.create({
    baseURL: BASE_API_URL,  
    withCredentials: false
});

export const getApi= (type) => {
    return api.get(type);
};