import {FETCH_SUCCESS, FETCH_PENDING, FETCH_ERROR} from './constant'

const initialState = {
    pending: false,
    results: [],
    error: null
}

export function cardReducer(state = initialState, action) {    
    switch(action.type) {
        case FETCH_PENDING: 
            return {
                ...state,
                pending: true
            }
        case FETCH_SUCCESS:
            return {
                ...state,
                pending: false,
                results: action.results
            }
        case FETCH_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}