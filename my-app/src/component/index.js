import React, {useEffect} from "react";
import { connect } from "react-redux";
import CustomCard  from './custom-card'
import Skeleton  from './skeleton'
import {fetchData} from './action';
import { bindActionCreators } from "redux";


const Component = ({match, results, fetchData, pending}) =>{

    
    let url = (match.url==='/')?'people/':match.url;

    useEffect(() => {        
      fetchData(url);      
    }, [fetchData, url]);
    
    let pageTitle = url.replace('/','') 
    let output = ''
      
    if(pending){
      output = <Skeleton title={pageTitle} totalCard={10}/>
    }else{    
      output = (<>{results.map((param) => <CustomCard parms={param} title={pageTitle}/>)}</>)
    }
    return output;


}

const mapStateToProps = state => ({
  results: state.cardReducer.results,
  pending: state.cardReducer.pending
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  fetchData: fetchData
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Component);
