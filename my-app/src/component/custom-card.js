import React from 'react'
import Card from 'react-bootstrap/Card'


const CustomCard = (props) => {   
  const params = props.parms   
    return(<Card >
    <Card.Body>
    <Card.Title >{props.title}</Card.Title>
      {Object.keys(params).map(key => {
        return (!Array.isArray(params[key]))?<CardText name={key} value = {params[key]} />:''})}   
    </Card.Body>
  </Card>)
}


const CardText =(props) => {  
   return(<Card.Text> {props.name}: {props.value}</Card.Text>);
}

export default CustomCard;