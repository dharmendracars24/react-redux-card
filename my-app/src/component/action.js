import { 
    FETCH_SUCCESS, 
    FETCH_PENDING, 
    FETCH_ERROR} from './constant'
  import {getApi } from '../service'
  
  
  export const fetchData = (url) => { 
     return  dispatch => {
       dispatch(fetchPending());   
       getApi(url).then(res => {        
            if(res.error) {
                throw(res.error);
            }                  
            dispatch(fetchSuccess(res.data.results));
            return res.data.results;
        })
        .catch(error => {
          dispatch(fetchFailure(error));
        })
      }
    
  }
  
  export const fetchSuccess = (data) => ({  
    type: FETCH_SUCCESS, 
    results: data 
  });
  
  export const fetchPending = () => ({
    type: FETCH_PENDING,  
  });
  
  export const fetchFailure = (error) => ({
    type: FETCH_ERROR,
    error: error
  });
  