import React from "react";

const Skeleton = (props) => {
    let returnCard = [];
    for (let i = 0; i < props.totalCard; i++) {
        returnCard.push(<SkeletonCard title={props.title} />)
    }
    return returnCard;
}

const SkeletonCard = (props) => {
     return(
        <div className="card">
        <div className="card-body">
           <div className="card-title h5">{props.title}</div>
            <div className="lines shine"></div>
            <div className="lines shine"></div>
            <div className="lines shine"></div>
            <div className="lines shine"></div>
            <div className="lines shine"></div>
            <div className="lines shine"></div>
            </div>            
       </div>)
} 

export default Skeleton;