import React from 'react'
import { BrowserRouter, withRouter}  from 'react-router-dom';
import { renderRoutes } from "react-router-config";
import Component from'./component/index'

const Root = ({ route }) => {  
    return (
        <BrowserRouter>    
            {renderRoutes(route.routes)}        
        </BrowserRouter>    
    );
};

export const routes = [
    {
      component: withRouter(Root),
      routes: [        
        {
            path: "/films",
            exact: true,
            component: Component
        },
        {
            path: "/planets",
            exact: true,
            component: Component
        },
        {
            path: "/",
            exact: true,
            component: Component
          }
      ]
    }
  ];

 
  